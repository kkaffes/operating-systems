# README #

### What is this repository for? ###

This repository contains my full work for Operating Systems course of ECE NTUA.

### How do I get set up? ###

Every exercise is self-contained and can be compiled just by using make.