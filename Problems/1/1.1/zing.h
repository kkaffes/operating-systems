/*                                                                       
 * zing.h
 * 
 * Simple example to demonstrate the linking of object files.
 * 
 *
 * Konstantinos Kaffes <kkaffes@gmail.com>
 *                                                                               
 */



#ifndef ZING_H__
#define ZING_H__

void zing(void);

#endif
