/*                                                                       
 * main.c
 * 
 * Simple example to demonstrate the linking of object files.
 * 
 *
 * Konstantinos Kaffes <kkaffes@gmail.com>
 *                                                                               
 */

#include "zing.h"

int main(){
	zing();
	return 0;
}

