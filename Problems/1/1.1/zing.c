/*                                                                       
 * zing.c
 * 
 * Simple example to demonstrate the linking of object files.
 * 
 *
 * Konstantinos Kaffes <kkaffes@gmail.com>
 *                                                                               
 */




#include <stdio.h>
#include <unistd.h>

void zing(void){
	printf("KABOOM\n");
}
