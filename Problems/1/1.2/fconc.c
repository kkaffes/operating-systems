/*                                                                       
 * fconc.c
 * 
 * Concatenates two input files to an output file.
 *
 * Usage: ./fconc infile1 infile2 [outfile (default:fconc.out)]
 *
 * Konstantinos Kaffes <kkaffes@gmail.com>
 *                                                                               
 */



#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>


void doWrite(int fd, const char *buff, int len){
	ssize_t ret1;
	ret1 = write(fd,buff,len);
	if (ret1<0){
		perror("write");
		exit(1);
	}
	int counter=ret1;
	while (counter<len){
		ret1 = write(fd,buff+counter,len-counter);
		if (ret1<0){
               		 perror("write");
               		 exit(1);
		}
		counter = counter+ret1;
        }
	 
	
}

void write_file(int fd, const char *infile){
	char buf[100];
	int fd1 = open(infile,O_RDONLY);
	if (fd1<0){
		perror(infile);
		exit(1);
	}
	ssize_t ret = read(fd1,buf,100);
	if (ret<0){
		perror("read");
		exit(1);
	}
	while (ret>0){
		doWrite(fd,buf,ret);
		ret = read(fd1,buf,100);
		if (ret<0){
			perror("read");
			exit(1);
		}
	}
	
	if (close(fd1)<0) {
		perror("close");
		exit(1);
	}
}





int main(int argc, char *argv[]){
	int fd;
	if((argc!=3)&&(argc!=4)){
		printf("Usage: ./fconc infile1 infile2 [outfile (default:fconc.out)]\n");
		exit(1);
	}
	if (argc==4){
		int i;
		for (i=1;i<argc-1;i++){
				if (strcmp(argv[i],argv[argc-1])==0){
					printf("Output file should have different name from all input files!\n");
					exit(1);
				}
		}
		int oflags = O_CREAT | O_WRONLY | O_TRUNC;
		int mode = S_IRUSR | S_IWUSR;
		fd = open(argv[3],oflags,mode);
		if (fd<0){
			perror(argv[3]);
			exit(1);
		}
		write_file(fd,argv[1]);
		write_file(fd,argv[2]);
		if (close(fd)<0) {
			perror("close");
			exit(1);
		}
		
		return 0;
	}
	else {
		char out_file[] = "fconc.out";
		int i;
		for (i=1;i<argc;i++){
				if (strcmp(argv[i],out_file)==0){
					printf("Output file should have different name from all input files!\nIn this case the name of the output file is the pretedermined 'fconc.out'\n");
					exit(1);
				}
		}
		//O_CREAT: Δημιουργία αρχείου αν δεν υπάρχει.
		//O_WRONLY: Εγγραφή (μόνο).
		//O_TRUNC: Μηδενισμός αρχείου αν υπάρχει.
		int oflags = O_CREAT | O_WRONLY | O_TRUNC;
		int mode = S_IRUSR | S_IWUSR;
		fd = open(out_file,oflags,mode);
		if (fd<0){
			perror(out_file);
			exit(1);
		}
		write_file(fd,argv[1]);
		write_file(fd,argv[2]);
		if (close(fd)<0) {
			perror("close");
			exit(1);
		}
		
		return 0;


	}
}
