/*                                                                       
 * fconc.c
 * 
 * Concatenates an arbitrary number of input files to an output file.
 *
 * Usage: ./fconcinf infile_1 [infile_2 ... infile_n] outfile
 *
 * Konstantinos Kaffes <kkaffes@gmail.com>
 *                                                                               
 */






#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>


void doWrite(int fd, const char *buff, int len){
	ssize_t ret1;
	ret1 = write(fd,buff,len);
	if (ret1<0){
		perror("write");
		exit(1);
	}
	if (ret1<len){
		printf("Less bytes were written than read and thus the program exits with an error");
		exit(1);
        }
}


void write_file(int fd, const char *infile){
	char buf[100];
	int fd1 = open(infile,O_RDONLY);
	if (fd1<0){
		perror(infile);
		exit(1);
	}
	ssize_t ret = read(fd1,buf,100);
	if (ret<0){
		perror("read");
		exit(1);
	}
	while (ret>0){
		doWrite(fd,buf,ret);
		ret = read(fd1,buf,100);
		if (ret<0){
			perror("read");
			exit(1);
		}
	}
	doWrite(fd,"\n",1);
	if (close(fd1)<0) {
		perror("close");
		exit(1);
	}
}





int main(int argc, char *argv[]){
	int fd;
	if((argc<3)){
		printf("Usage: ./fconcinf infile_1 [infile_2 ... infile_n] outfile\n");
		exit(1);
	}
	int i;
	for (i=1;i<argc-1;i++){
		if (strcmp(argv[i],argv[argc-1])==0){
			printf("Output file should have different name from all input files!\n");
			exit(1);
		}
	}
	//O_CREAT : Δημιουργία αρχείου αν δεν υπάρχει.
	//O_WRONLY: Εγγραφή (μόνο).
	//O_TRUNC : Μηδενισμός αρχείου αν υπάρχει.
	int oflags = O_CREAT | O_WRONLY | O_TRUNC;
	int mode = S_IRUSR | S_IWUSR;
	fd = open(argv[argc-1],oflags,mode);
	if (fd<0){
		perror(argv[argc-1]);
		exit(1);
	}
	for (i=1; i<argc-1;i++){
		write_file(fd,argv[i]);
	}
	if (close(fd)<0) {
		perror("close");
		exit(1);
	}
		
	return 0;
	
}
