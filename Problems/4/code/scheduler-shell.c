#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <assert.h>

#include <sys/wait.h>
#include <sys/types.h>

#include "proc-common.h"
#include "request.h"

/* Compile-time parameters. */
#define SCHED_TQ_SEC 2                /* time quantum */
#define TASK_NAME_SZ 60               /* maximum size for a task's name */
#define SHELL_EXECUTABLE_NAME "shell" /* executable for shell */

int i;

struct node
{	
	 //int killed = 0 ;
	 char *name;
     int data;
     int id;
     struct node *next;
}*curr,*last,*var;


/* Disable delivery of SIGALRM and SIGCHLD. */
static void
signals_disable(void)
{
	sigset_t sigset;

	sigemptyset(&sigset);
	sigaddset(&sigset, SIGALRM);
	sigaddset(&sigset, SIGCHLD);
	if (sigprocmask(SIG_BLOCK, &sigset, NULL) < 0) {
		perror("signals_disable: sigprocmask");
		exit(1);
	}
}

/* Enable delivery of SIGALRM and SIGCHLD.  */
static void
signals_enable(void)
{
	sigset_t sigset;

	sigemptyset(&sigset);
	sigaddset(&sigset, SIGALRM);
	sigaddset(&sigset, SIGCHLD);
	if (sigprocmask(SIG_UNBLOCK, &sigset, NULL) < 0) {
		perror("signals_enable: sigprocmask");
		exit(1);
	}
}



/* Print a list of all tasks currently being scheduled.  */
static void
sched_print_tasks(void)
{
	int temp_id = curr->id;
	struct node *temp;
	temp = curr;
	printf("Process with name %s, PID %d and ID %d  \n", temp->name, temp->data, temp->id);
	temp = curr->next;
	while(temp->id != temp_id){
		printf("Process with name %s, PID %d and ID %d  \n", temp->name, temp->data, temp->id);
		temp = temp->next;
	}
}

/* Send SIGKILL to a task determined by the value of its
 * scheduler-specific id.
 */
static int
sched_kill_task_by_id(int id)
{	
	struct node *temp;
	if (id == 1){
		printf("The shell doesn't have suicidal thoughts and thus doesn't want to kill itself.\n ACTION ABORTED!!!\n");
		return 0;
	}
	else{
		
		temp = curr;

		if (temp->id == id){
			kill(temp->data,SIGKILL);
			return 0;
		}
		temp = temp->next;
		while(temp->id!=curr->id){
			if (temp->id == id){
				kill(temp->data,SIGKILL);
				return 0;
				break;
			}
			temp = temp->next;
		}

	}
	return -ENOSYS;
}


/* Create a new task.  */
static void
sched_create_task(char *executable)
{
	pid_t pid = fork();

		if (pid<0){
			perror("fork");
			exit(1);
		}

		if (pid==0){
			char *newargv[] = { executable, NULL, NULL, NULL };
			char *newenviron[] = { NULL };

			
			
			raise(SIGSTOP);

			execve(executable, newargv, newenviron);

			/* execve() only returns on error */
			perror("execve");
			exit(1);

		}

		var=(struct node *)malloc(sizeof(struct node));
     	var->data=pid;
     	var->id = ++i;
     	var->name = strdup(executable);
     	var->next = curr->next;
     	curr->next = var;
     	//last->next = var;
     	//last=var;


}

/* Process requests by the shell.  */
static int
process_request(struct request_struct *rq)
{
	switch (rq->request_no) {
		case REQ_PRINT_TASKS:
			sched_print_tasks();
			return 0;

		case REQ_KILL_TASK:
			return sched_kill_task_by_id(rq->task_arg);

		case REQ_EXEC_TASK:
			sched_create_task(rq->exec_task_arg);
			return 0;

		default:
			return -ENOSYS;
	}
}

/* 
 * SIGALRM handler
 */
static void
sigalrm_handler(int signum)
{
	//Check if the signal received is the right one
	if (signum != SIGALRM) {
                fprintf(stderr, "Internal error: Called for signum %d, not SIGALRM\n",
                        signum);
                exit(1);
        }

	//Stop the current process when a SIGALRM signal is received
    printf("ALARM! %d seconds have passed.\n", SCHED_TQ_SEC);
	kill (curr->data,SIGSTOP);
}

/* 
 * SIGCHLD handler
 */
static void
sigchld_handler(int signum)
{
	int status;
	pid_t p;
	struct node *temp,*junk;
	if (signum != SIGCHLD) {
                fprintf(stderr, "Internal error: Called for signum %d, not SIGCHLD\n",
                        signum);
                exit(1);
        }


    while (1){
    	p = waitpid(-1, &status, WUNTRACED | WNOHANG);
    	if (p==0)
    		break;
    	explain_wait_status(p, status);
    	
    	if (WIFEXITED(status)||WIFSIGNALED(status)){
    		if (curr->next != curr)
    		{
    			

    			if (curr->data == p){
    				temp = curr;
    				while(temp->next->data!=p){
    					temp = temp->next;
    				}
    				temp->next = temp->next->next;
    				free(curr);
    				curr = temp->next;

    				if (alarm(SCHED_TQ_SEC) < 0) {
                    	perror("alarm");
                    	exit(1);
                	}
					/*"Awake" next procces in the queue*/
					kill(curr->data,SIGCONT);
    			}
    			else{
    				
    				temp = curr;
    				while(temp->next->data!=p){
    					temp = temp->next;
    				}
    				junk = temp->next;
    				temp->next = temp->next->next;
    				free(junk);
    				
    			}
    			
    		}
    		else {
    			printf("The final process is over. The scheduler is exiting!\n");
    			exit(1);
			}
		}
	
	
		if (WIFSTOPPED(status)){
			if (curr->data == p){

				if (alarm(SCHED_TQ_SEC) < 0) {
            		perror("alarm");
            		exit(1);
        		}

				curr = curr->next;
				kill (curr->data,SIGCONT);
			}
			
		}
	}
}




/* Install two signal handlers.
 * One for SIGCHLD, one for SIGALRM.
 * Make sure both signals are masked when one of them is running.
 */
static void
install_signal_handlers(void)
{
	sigset_t sigset;
	struct sigaction sa;

	sa.sa_handler = sigchld_handler;
	sa.sa_flags = SA_RESTART;
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGCHLD);
	sigaddset(&sigset, SIGALRM);
	sa.sa_mask = sigset;
	if (sigaction(SIGCHLD, &sa, NULL) < 0) {
		perror("sigaction: sigchld");
		exit(1);
	}

	sa.sa_handler = sigalrm_handler;
	if (sigaction(SIGALRM, &sa, NULL) < 0) {
		perror("sigaction: sigalrm");
		exit(1);
	}

	/*
	 * Ignore SIGPIPE, so that write()s to pipes
	 * with no reader do not result in us being killed,
	 * and write() returns EPIPE instead.
	 */
	if (signal(SIGPIPE, SIG_IGN) < 0) {
		perror("signal: sigpipe");
		exit(1);
	}
}

static void
do_shell(char *executable, int wfd, int rfd)
{
	char arg1[10], arg2[10];
	char *newargv[] = { executable, NULL, NULL, NULL };
	char *newenviron[] = { NULL };

	sprintf(arg1, "%05d", wfd);
	sprintf(arg2, "%05d", rfd);
	newargv[1] = arg1;
	newargv[2] = arg2;

	raise(SIGSTOP);
	execve(executable, newargv, newenviron);

	/* execve() only returns on error */
	perror("scheduler: child: execve");
	exit(1);
}

/* Create a new shell task.
 *
 * The shell gets special treatment:
 * two pipes are created for communication and passed
 * as command-line arguments to the executable.
 */
static void
sched_create_shell(char *executable, int *request_fd, int *return_fd)
{
	pid_t p;
	int pfds_rq[2], pfds_ret[2];

	if (pipe(pfds_rq) < 0 || pipe(pfds_ret) < 0) {
		perror("pipe");
		exit(1);
	}

	p = fork();
	if (p < 0) {
		perror("scheduler: fork");
		exit(1);
	}

	if (p == 0) {
		/* Child */
		close(pfds_rq[0]);
		close(pfds_ret[1]);
		do_shell(executable, pfds_rq[1], pfds_ret[0]);
		assert(0);
	}

	curr = (struct node *)malloc(sizeof(struct node));
	curr->data = p;
	curr->id = 1;
	curr->name = "Shell";
	curr->next = curr;
	last = curr;

	/* Parent */
	close(pfds_rq[1]);
	close(pfds_ret[0]);
	*request_fd = pfds_rq[0];
	*return_fd = pfds_ret[1];
}

static void
shell_request_loop(int request_fd, int return_fd)
{
	int ret;
	struct request_struct rq;

	/*
	 * Keep receiving requests from the shell.
	 */
	for (;;) {
		if (read(request_fd, &rq, sizeof(rq)) != sizeof(rq)) {
			perror("scheduler: read from shell");
			fprintf(stderr, "Scheduler: giving up on shell request processing.\n");
			break;
		}

		signals_disable();
		ret = process_request(&rq);
		signals_enable();

		if (write(return_fd, &ret, sizeof(ret)) != sizeof(ret)) {
			perror("scheduler: write to shell");
			fprintf(stderr, "Scheduler: giving up on shell request processing.\n");
			break;
		}
	}
}

int main(int argc, char *argv[])
{
	pid_t pid;
	int nproc;
	/* Two file descriptors for communication with the shell */
	static int request_fd, return_fd;

	/* Create the shell. */
	sched_create_shell(SHELL_EXECUTABLE_NAME, &request_fd, &return_fd);
	/* TODO: add the shell to the scheduler's tasks */

 

	/*
	 * For each of argv[1] to argv[argc - 1],
	 * create a new child process, add it to the process list.
	 */


	 for(i=1;i<argc;i++){


		pid = fork();

		if (pid<0){
			perror("fork");
			exit(1);
		}

		if (pid==0){
			char *newargv[] = { argv[i], NULL, NULL, NULL };
			char *newenviron[] = { NULL };

			printf("I am %s, PID = %ld\n",
				argv[0], (long)getpid());
			printf("About to replace myself with the executable %s...\n",
				argv[i]);

			raise(SIGSTOP);

			execve(argv[i], newargv, newenviron);

			/* execve() only returns on error */
			perror("execve");
			exit(1);

		}


		var=(struct node *)malloc(sizeof(struct node));
     	var->data=pid;
     	var->id = i+1;
     	var->name = argv[i];
     	var->next = curr;
     	last->next = var;
     	last=var;

	}



	nproc = argc; /* number of proccesses goes here */

	/* Wait for all children to raise SIGSTOP before exec()ing. */
	wait_for_ready_children(nproc);

	/* Install SIGALRM and SIGCHLD handlers. */
	install_signal_handlers();

	if (nproc == 0) {
		fprintf(stderr, "Scheduler: No tasks. Exiting...\n");
		exit(1);
	}


	if (alarm(SCHED_TQ_SEC) < 0) {
                    perror("alarm");
                    exit(1);
    }


	kill(curr->data,SIGCONT);

	shell_request_loop(request_fd, return_fd);

	/* Now that the shell is gone, just loop forever
	 * until we exit from inside a signal handler.
	 */
	while (pause())
		;

	/* Unreachable */
	fprintf(stderr, "Internal error: Reached unreachable point\n");
	return 1;
}
