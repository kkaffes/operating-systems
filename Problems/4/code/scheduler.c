#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <assert.h>

#include <sys/wait.h>
#include <sys/types.h>

#include "proc-common.h"
#include "request.h"

/* Compile-time parameters. */
#define SCHED_TQ_SEC 2                /* time quantum */
#define TASK_NAME_SZ 60               /* maximum size for a task's name */


struct node
{
     int data;
     struct node *next;
}*curr;

/*
 * SIGALRM handler
 */
static void
sigalrm_handler(int signum)
{
	
	//Check if the signal received is the right one
	if (signum != SIGALRM) {
                fprintf(stderr, "Internal error: Called for signum %d, not SIGALRM\n",
                        signum);
                exit(1);
        }

	//Stop the current process when a SIGALRM signal is received
    printf("ALARM! %d seconds have passed.\n", SCHED_TQ_SEC);
	kill (curr->data,SIGSTOP);

}

/* 
 * SIGCHLD handler
 */
static void
sigchld_handler(int signum)
{
	int status;
	pid_t p;
	struct node *temp,*junk;
	if (signum != SIGCHLD) {
                fprintf(stderr, "Internal error: Called for signum %d, not SIGCHLD\n",
                        signum);
                exit(1);
        }


    while (1){
    	p = waitpid(-1, &status, WUNTRACED | WNOHANG);
    	if (p==0)
    		break;
    	explain_wait_status(p, status);
    	
    	if (WIFEXITED(status)||WIFSIGNALED(status)){
    		if (curr->next != curr)
    		{
    			

    			if (curr->data == p){
    				temp = curr;
    				while(temp->next->data!=p){
    					temp = temp->next;
    				}
    				temp->next = temp->next->next;
    				free(curr);
    				curr = temp->next;

    				if (alarm(SCHED_TQ_SEC) < 0) {
                    	perror("alarm");
                    	exit(1);
                	}
					/*"Awake" next procces in the queue*/
					kill(curr->data,SIGCONT);
    			}
    			else{
    				
    				temp = curr;
    				while(temp->next->data!=p){
    					temp = temp->next;
    				}
    				junk = temp->next;
    				temp->next = temp->next->next;
    				free(junk);
    				
    			}
    			
    		}
    		else {
    			printf("The final process is over. The scheduler is exiting!\n");
    			exit(1);
			}
		}
	
	
		if (WIFSTOPPED(status)){
			if (alarm(SCHED_TQ_SEC) < 0) {
            	perror("alarm");
            	exit(1);
        	}

			curr = curr->next;
			kill (curr->data,SIGCONT);
		}
	}
}

/* Install two signal handlers.
 * One for SIGCHLD, one for SIGALRM.
 * Make sure both signals are masked when one of them is running.
 */
static void
install_signal_handlers(void)
{
	sigset_t sigset;
	struct sigaction sa;

	
	sa.sa_flags = SA_RESTART;
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGCHLD);
	sigaddset(&sigset, SIGALRM);
	sa.sa_mask = sigset;
	sa.sa_handler = sigchld_handler;
	if (sigaction(SIGCHLD, &sa, NULL) < 0) {
		perror("sigaction: sigchld");
		exit(1);
	}

	sa.sa_handler = sigalrm_handler;
	if (sigaction(SIGALRM, &sa, NULL) < 0) {
		perror("sigaction: sigalrm");
		exit(1);
	}

	/*
	 * Ignore SIGPIPE, so that write()s to pipes
	 * with no reader do not result in us being killed,
	 * and write() returns EPIPE instead.
	 */
	if (signal(SIGPIPE, SIG_IGN) < 0) {
		perror("signal: sigpipe");
		exit(1);
	}
}

int main(int argc, char *argv[])
{
	int nproc,i;
	pid_t pid;
	struct node *var,*last;
	/*
	 * For each of argv[1] to argv[argc - 1],
	 * create a new child process, add it to the process list.
	 */

	 if (argc == 2){

	 	pid = fork();

		if (pid<0){
			perror("fork");
			exit(1);
		}

		if (pid==0){
			char *newargv[] = { argv[1], NULL, NULL, NULL };
			char *newenviron[] = { NULL };

			printf("I am %s, PID = %ld\n",
				argv[0], (long)getpid());
			printf("About to replace myself with the executable %s...\n",
				argv[1]);

			raise(SIGSTOP);

			execve(argv[1], newargv, newenviron);

			/* execve() only returns on error */
			perror("execve");
			exit(1);

		}

		curr = (struct node *)malloc(sizeof(struct node));
		curr->data = pid;
		curr->next = curr;
		last = curr;

	 }

	 else{


		for(i=1;i<argc;i++){


			pid = fork();

			if (pid<0){
				perror("fork");
				exit(1);
			}

			if (pid==0){
				char *newargv[] = { argv[i], NULL, NULL, NULL };
				char *newenviron[] = { NULL };

				printf("I am %s, PID = %ld\n",
					argv[0], (long)getpid());
				printf("About to replace myself with the executable %s...\n",
					argv[i]);

				raise(SIGSTOP);

				execve(argv[i], newargv, newenviron);

				/* execve() only returns on error */
				perror("execve");
				exit(1);

			}

			if (i==1){
				curr = (struct node *)malloc(sizeof(struct node));
				curr->data = pid;
				curr->next = NULL;
				last = curr;
			}
			else if (i<argc-1){
				var=(struct node *)malloc(sizeof(struct node));
     			var->data=pid;
     			var->next = NULL;
     			last->next = var;
     			last=var;
     		}
     		else{
     			var=(struct node *)malloc(sizeof(struct node));
     			var->data=pid;
     			var->next = curr;
     			last->next = var;
     			last=var;
     		}
		} 
	}



	nproc = argc-1; /* number of proccesses goes here */


	/* Wait for all children to raise SIGSTOP before exec()ing. */
	wait_for_ready_children(nproc);

	/* Install SIGALRM and SIGCHLD handlers. */
	install_signal_handlers();

	if (nproc == 0) {
		fprintf(stderr, "Scheduler: No tasks. Exiting...\n");
		exit(1);
	}

	if (alarm(SCHED_TQ_SEC) < 0) {
        perror("alarm");
        exit(1);
    }

    kill(curr->data,SIGCONT);
	/* loop forever  until we exit from inside a signal handler. */
	while (pause())
		;

	/* Unreachable */
	fprintf(stderr, "Internal error: Reached unreachable point\n");
	return 1;
}
