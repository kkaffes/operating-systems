#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "tree.h"
#include "proc-common.h"

void fork_procs(struct tree_node *cur_root)
{
	/*
	 * Start
	 */
    printf("PID = %ld, name %s, starting...\n",
	(long)getpid(), cur_root->name);
    

    pid_t *p_cur;
    int counter;

    //Allocating pIDs for the children of the current node
    p_cur = malloc(sizeof(pid_t)*cur_root->nr_children);
    if (p_cur == NULL){
            fprintf(stderr, "allocate PIDs failed\n");
            exit(1);
    }

   
    

    //Changing name for process tree
    change_pname(cur_root->name);
    

    //Generating all the children of the process
    for (counter=0;counter<(cur_root->nr_children);counter++)
    {
        fprintf(stderr, "Parent, PID = %ld: Creating child...\n",
                (long)getpid());
        p_cur[counter] = fork();
        if (p_cur[counter] < 0) {
                /* fork failed */
                perror("fork");
                exit(1);
        }
        if (p_cur[counter] == 0) {
                /* Child */
                fork_procs(cur_root->children+counter);
                exit(1);
        }
        fprintf(stderr,"Parent, PID = %ld: Created child with PID = %ld, waiting for it to terminate...\n",
                (long)getpid(), (long)p_cur[counter]);       

	}

    

    /*
    * In parent process. Wait for the child to terminate
    * and report its termination status.
    */

    wait_for_ready_children(cur_root->nr_children);
    raise(SIGSTOP);
	
	printf("PID = %ld, name = %s is awake\n",
		(long)getpid(), cur_root->name);
    int status;
	for(counter=0;counter<(cur_root->nr_children);counter++)
	{
		
		kill(p_cur[counter], SIGCONT);
		wait(&status);
		//explain_wait_status(pid, status);

	}

	/*
	 * Exit
	 */
	exit(0);
}

/*
 * The initial process forks the root of the process tree,
 * waits for the process tree to be completely created,
 * then takes a photo of it using show_pstree().
 *
 * How to wait for the process tree to be ready?
 * In ask2-{fork, tree}:
 *      wait for a few seconds, hope for the best.
 * In ask2-signals:
 *      use wait_for_ready_children() to wait until
 *      the first process raises SIGSTOP.
 */

int main(int argc, char *argv[])
{
	pid_t pid;
	int status;
	struct tree_node *root;

	if (argc < 2){
		fprintf(stderr, "Usage: %s <tree_file>\n", argv[0]);
		exit(1);
	}

	/* Read tree into memory */
	root = get_tree_from_file(argv[1]);

	/* Fork root of process tree */
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		/* Child */
		fork_procs(root);
		exit(1);
	}

	/*
	 * Father
	 */
	wait_for_ready_children(1);

	/* for ask2-{fork, tree} */
	/* sleep(SLEEP_TREE_SEC); */

	/* Print the process tree root at pid */
	show_pstree(pid);

	/* for ask2-signals */
	kill(pid, SIGCONT);

	/* Wait for the root of the process tree to terminate */
	wait(&status);
	explain_wait_status(pid, status);

	return 0;
}
