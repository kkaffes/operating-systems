#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "proc-common.h"
#include "tree.h"

#define SLEEP_SEC  10
#define SLEEP_TREE_SEC  3

/*
 * Create this process tree:
 * A-+-B---D
 *   `-C
 */
void fork_procs(struct tree_node *cur_root)
{
	
    pid_t p_cur;
    int counter,status;

    change_pname(cur_root->name);
    
    for (counter=0;counter<(cur_root->nr_children);counter++)
    {
        fprintf(stderr, "Parent, PID = %ld: Creating child...\n",
                (long)getpid());
        p_cur = fork();
        if (p_cur < 0) {
                /* fork failed */
                perror("fork");
                exit(1);
        }
        if (p_cur == 0) {
                /* Child */
                fork_procs(cur_root->children+counter);
                exit(1);
        }
        fprintf(stderr,"Parent, PID = %ld: Created child with PID = %ld, waiting for it to terminate...\n",
                (long)getpid(), (long)p_cur);       

	}

    

    /*
    * In parent process. Wait for the child to terminate
    * and report its termination status.
    */
    for (counter=0; counter<(cur_root->nr_children); counter++){
        p_cur=wait(&status);
        explain_wait_status(p_cur,status);
    }
    
    if (cur_root->nr_children==0)
        sleep(SLEEP_SEC);        
	   
	exit(1);
}

/*
 * The initial process forks the root of the process tree,
 * waits for the process tree to be completely created,
 * then takes a photo of it using show_pstree().
 *
 * How to wait for the process tree to be ready?
 * In ask2-{fork, tree}:
 *      wait for a few seconds, hope for the best.
 * In ask2-signals:
 *      use wait_for_ready_children() to wait until
 *      the first process raises SIGSTOP.
 */
int main(int argc, char *argv[])
{
	
    struct tree_node *root;
    //Process Id of the root
    pid_t pid;                  
    int status;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <input_tree_file>\n\n", argv[0]);
        exit(1);
    }

    //Parsing the tree-file
    root = get_tree_from_file(argv[1]);

	/* Fork root of process tree */
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		/* Child */
		fork_procs(root);
		exit(1);
	}

	/*
	 * Father
	 */
	/* for ask2-signals */
	/* wait_for_ready_children(1); */

	/* for ask2-{fork, tree} */
	sleep(SLEEP_TREE_SEC);

	/* Print the process tree root at pid */
	show_pstree(pid);

	/* for ask2-signals */
	/* kill(pid, SIGCONT); */

	/* Wait for the root of the process tree to terminate */
	pid = wait(&status);
	explain_wait_status(pid, status);

	return 0;
}
