





#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "proc-common.h"

#define SLEEP_SEC  60
#define SLEEP_TREE_SEC  3

/*
 * Create this process tree:
 * A-+-B---D
 *   `-C
 */
void fork_procs(void)
{
	/*
	 * initial process is A.
	 */

	pid_t p1,p2;
        int status1,status2;

        fprintf(stderr, "Parent, PID = %ld: Creating child...\n",
                (long)getpid());
        p1 = fork();
        if (p1 < 0) {
                /* fork failed */
                perror("fork");
                exit(1);
        }
        if (p1 == 0) {
                /* In child process */
                change_pname("B");
                pid_t p3;
                int status3;
                fprintf(stderr, "Parent, PID = %ld: Creating child...\n",(long)getpid());
                p3 = fork();
                if (p3 < 0) {
                        /*fork failed*/
                        perror("fork");
                        exit(1);
                }
                if (p3 == 0) {
                        change_pname("D");
                        sleep(SLEEP_SEC);
                        exit(13);
                }
                printf("Parent, PID = %ld: Created child with PID = %ld, waiting for it to terminate...\n",(long)getpid(), (long)p3);
                p3 = wait(&status3);
                explain_wait_status(p3,status3);
                exit(19);

	 }

        change_pname("A");
        printf("Parent, PID = %ld: Created child with PID = %ld, waiting for it to terminate...\n",
                (long)getpid(), (long)p1);

        fprintf(stderr, "Parent, PID = %ld: Creating child...\n",
                (long)getpid());

        p2 = fork();
        if (p2 < 0){
                /*fork failed*/
                perror("fork");
                exit(1);
        }
        if (p2 == 0){
                /*In child process*/
                change_pname("C");
                sleep(SLEEP_SEC);
                exit(17);
        }

        /*
         * In parent process. Wait for the child to terminate
         * and report its termination status.
         */
        printf("Parent, PID = %ld: Created child with PID = %ld, waiting for it to terminate...\n",
                (long)getpid(), (long)p2);

        p1 = wait(&status1);
        explain_wait_status(p1, status1);

        p2 = wait(&status2);
        explain_wait_status(p2, status2);
	printf("A: Exiting...\n");
	exit(16);
}

/*
 * The initial process forks the root of the process tree,
 * waits for the process tree to be completely created,
 * then takes a photo of it using show_pstree().
 *
 * How to wait for the process tree to be ready?
 * In ask2-{fork, tree}:
 *      wait for a few seconds, hope for the best.
 * In ask2-signals:
 *      use wait_for_ready_children() to wait until

 *      the first process raises SIGSTOP.
 */
int main(void)
{
	pid_t pid;
	int status;

	/* Fork root of process tree */
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		/* Child */
		fork_procs();
		exit(1);
	}

	/*
	 * Father
	 */
	/* for ask2-signals */
	/* wait_for_ready_children(1); */

	/* for ask2-{fork, tree} */
	sleep(SLEEP_TREE_SEC);

	/* Print the process tree root at pid */
	show_pstree(pid);

	/* for ask2-signals */
	/* kill(pid, SIGCONT); */

	/* Wait for the root of the process tree to terminate */
	pid = wait(&status);
	explain_wait_status(pid, status);

	return 0;
}
