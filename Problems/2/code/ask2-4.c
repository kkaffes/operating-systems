#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

#include "tree.h"
#include "proc-common.h"

void fork_procs(struct tree_node *cur_root,int *pfd)
{
	/*
	 * Start
	 */
	long num,num1,num2;
	pid_t p_cur;
	int status;
    
	//printf("Entering the process.I am %s \n",cur_root->name);
    if (cur_root->nr_children==0){
    	char *end_ptr;
    	num = strtol(cur_root->name,&end_ptr,10);
    	if (write(pfd[1],&num,sizeof(num))!=sizeof(num)){
    		perror("write to pipe");
    		exit(1);
    	}
    	//printf("Returning %ld. I am %s \n", num, cur_root->name);
    	exit(1);
    }
    else{
    	int my_pfd[2];

    	if (pipe(my_pfd)<0){
			perror("pipe");
			exit(1);
		}

		//printf("Creating the 2 children.I am %s \n",cur_root->name);
    	p_cur = fork();
        if (p_cur < 0) {
        	/* fork failed */
         	perror("fork");
         	exit(1);
        }
        if (p_cur == 0) {
            /* Child */
                fork_procs(cur_root->children,my_pfd);
                exit(1);
        }

    	p_cur = fork();
        if (p_cur < 0) {
        	/* fork failed */
         	perror("fork");
         	exit(1);
        }
        if (p_cur == 0) {
            /* Child */
                fork_procs(cur_root->children+1,my_pfd);
                exit(1);
        }

    	
    
		if(read(my_pfd[0],&num1,sizeof(num1))!=sizeof(num1)){
			perror("read from pipe");
			exit(1);
		}

		if(read(my_pfd[0],&num2,sizeof(num2))!=sizeof(num2)){
			perror("read from pipe");
			exit(1);
		}
		//printf("I have read the numbers. I am %s\n", cur_root->name);
		if ((cur_root->name)[0]=='*')
			num = num1*num2;

		if ((cur_root->name)[0]=='+')
			num = num1+num2;

		if (write(pfd[1],&num,sizeof(num))!=sizeof(num)){
    		perror("write to pipe");
    		exit(1);
    	}
    	
    	p_cur=wait(&status);
        explain_wait_status(p_cur,status);
    	p_cur=wait(&status);
    	explain_wait_status(p_cur,status);

    	exit(0);

   
	}
}

/*
 * The initial process forks the root of the process tree,
 * waits for the process tree to be completely created,
 * then takes a photo of it using show_pstree().
 *
 * How to wait for the process tree to be ready?
 * In ask2-{fork, tree}:
 *      wait for a few seconds, hope for the best.
 * In ask2-signals:
 *      use wait_for_ready_children() to wait until
 *      the first process raises SIGSTOP.
 */

int main(int argc, char *argv[])
{
	int pfd[2];
	pid_t pid;
	long value;
	struct tree_node *root;
	int status;

	if (argc < 2){
		fprintf(stderr, "Usage: %s <tree_file>\n", argv[0]);
		exit(1);
	}

	/* Read tree into memory */
	root = get_tree_from_file(argv[1]);
	check_arithmetic_tree(root);
	if (pipe(pfd)<0){
		perror("pipe");
		exit(1);
	}


	/* Fork root of process tree */
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		/* Child */
		fork_procs(root,pfd);
		exit(1);
	}

	/*
	 * Father
	 */
	if(read(pfd[0],&value,sizeof(value))!=sizeof(value)){
		perror("read from pipe");
		exit(1);
	}

	pid=wait(&status);
        explain_wait_status(pid,status);

	printf("The final result is: %ld\n", value);




	return 0;
}
