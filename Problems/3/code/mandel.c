/*
 * mandel.c
 *
 * A program to draw the Mandelbrot Set on a 256-color xterm.
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <semaphore.h>
#include <signal.h>
#include "mandel-lib.h"

#define MANDEL_MAX_ITERATION 100000


#define perror_pthread(ret, msg) \
	do { errno = ret; perror(msg); } while (0)


/***************************
 * Compile-time parameters *
 ***************************/

/*
 * Output at the terminal is is x_chars wide by y_chars long
*/
int y_chars = 50;
int x_chars = 90;

/*
 * The part of the complex plane to be drawn:
 * upper left corner is (xmin, ymax), lower right corner is (xmax, ymin)
*/
double xmin = -1.8, xmax = 1.0;
double ymin = -1.0, ymax = 1.0;
	
/*
 * Every character in the final output is
 * xstep x ystep units wide on the complex plane.
 */
double xstep;
double ystep;

/*
 * This function computes a line of output
 * as an array of x_char color values.
 */
static void signal_handler(int sig){
    if (sig == SIGINT){
	reset_xterm_color(1);
	exit(1);
    }
}

struct thread_info_struct {
	pthread_t tid; /* POSIX thread id, as returned by the library */
	sem_t *sem_list;
	
	
	int thrid; /* Application-defined thread id */
	int thrcnt;
};


void compute_mandel_line(int line, int color_val[])
{
	/*
	 * x and y traverse the complex plane.
	 */
	double x, y;

	int n;
	int val;

	/* Find out the y value corresponding to this line */
	y = ymax - ystep * line;

	/* and iterate for all points on this line */
	for (x = xmin, n = 0; n <= x_chars; x+= xstep, n++) {

		/* Compute the point's color value */
		val = mandel_iterations_at_point(x, y, MANDEL_MAX_ITERATION);
		if (val > 255)
			val = 255;

		/* And store it in the color_val[] array */
		val = xterm_color(val);
		color_val[n] = val;
	}
}

/*
 * This function outputs an array of x_char color values
 * to a 256-color xterm.
 */
void output_mandel_line(int fd, int color_val[])
{
	int i;
	
	char point ='@';
	char newline='\n';

	for (i = 0; i < x_chars; i++) {
		/* Set the current color, then output the point */
		set_xterm_color(fd, color_val[i]);
		if (write(fd, &point, 1) != 1) {
			perror("compute_and_output_mandel_line: write point");
			exit(1);
		}
	}

	/* Now that the line is done, output a newline character */
	if (write(fd, &newline, 1) != 1) {
		perror("compute_and_output_mandel_line: write newline");
		exit(1);
	}
}

void compute_and_output_mandel_line(int fd, int line, struct thread_info_struct *thr)
{
	/*
	 * A temporary array, used to hold color values for the line being drawn
	 */
	int color_val[x_chars];

	

	compute_mandel_line(line, color_val);
	sem_wait(&thr->sem_list[thr->thrid]);
	output_mandel_line(fd, color_val);
	sem_post(&thr->sem_list[(thr->thrid + 1)%(thr->thrcnt)]);
}


int safe_atoi(char *s, int *val)
{
	long l;
	char *endp;

	l = strtol(s, &endp, 10);
	if (s != endp && *endp == '\0') {
		*val = l;
		return 0;
	} else
		return -1;
}


void *safe_malloc(size_t size)
{
	void *p;

	if ((p = malloc(size)) == NULL) {
		fprintf(stderr, "Out of memory, failed to allocate %zd bytes\n",
			size);
		exit(1);
	}

	return p;
}


void *thread_start_fn(void *arg)
{
	struct thread_info_struct *thr = arg;

	int line;
	for (line = thr->thrid; line < y_chars; line+=thr->thrcnt) {
		compute_and_output_mandel_line(1, line, thr);
	}
	return NULL;
}

int main(int argc, char *argv[])
{

	int N,i,ret;	
	struct thread_info_struct *thr;


	if(argc!=2){
		fprintf(stderr, "mandel takes only one argument, the number of threads to be created\n");
		exit(1);	
	}

	if (safe_atoi(argv[1], &N) < 0 || N <= 0) {
		fprintf(stderr, "`%s' is not valid for `number of threads'\n", argv[1]);
		exit(1);
	}
	
	struct sigaction sa;
	sigemptyset(&sa.sa_mask);

	sa.sa_flags = 0;
	sa.sa_handler = signal_handler;
	if (sigaction(SIGINT,&sa,NULL)==-1)
	{
		perror("sigaction");
		exit(1);
	}
	xstep = (xmax - xmin) / x_chars;
	ystep = (ymax - ymin) / y_chars;

	sem_t *my_sem;
	my_sem = safe_malloc(N * sizeof(*my_sem));
	
	sem_init(&my_sem[0], 0, 1);
	for (i=1; i < N; i++)
		sem_init(&my_sem[i],0,0);
			

	thr = safe_malloc(N * sizeof(*thr));

	for (i = 0; i < N; i++) {
		/* Initialize per-thread structure */
		thr[i].thrid = i;
		thr[i].thrcnt = N;
		thr[i].sem_list = my_sem;
		/* Spawn new thread */
		ret = pthread_create(&thr[i].tid, NULL, thread_start_fn, &thr[i]);
		if (ret) {
			perror_pthread(ret, "pthread_create");
			exit(1);
		}
	}

	

	/*
	 * Wait for all threads to terminate
	 */
	for (i = 0; i < N; i++) {
		ret = pthread_join(thr[i].tid, NULL);
		if (ret) {
			perror_pthread(ret, "pthread_join");
			exit(1);
		}
	}
	

	reset_xterm_color(1);
	return 0;
}
